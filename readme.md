# An HTML5 Prime Spiral (Ulam Spiral) Plotter 

## To Use

- Clone or download the files.
- Open the HTML file in your browser.
- Set the controls as desired
- Click 'Calculate'
- Tweak the HTML/Coffescript/Javascript as you see fit...

See here for more about the [Ulam Spiral](en.wikipedia.org/wiki/Ulam_spiral).

![User Interface](http://farm9.staticflickr.com/8053/8410514395_2ae234a120.jpg)
