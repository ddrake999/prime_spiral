tri = false       # flag whether triangular or square
canvWidth = 600   # canvas dimension width=height
pixel = 1         # size of a 'pixel'
canvTop = null    # canvas top and left position (for getting coordinates).
canvLeft = null

numbers = [0,0]   # the factors for the number of the index.  Indices 0, and 1 are placeholders
points = []       # the screen coords of the number of the index 
primes = []       # a simple array of primes
server_present = typeof jQuery != 'undefined'  # If we're using the server, we'll have jquery for ajax
max_num = null    # the number of points to plot (depends on canvWidth and tri)

# hash with key a string representation of the x,y coords 
# and value the prime number at those coords.
point_primes = {}

# The Canvas and Graphics context.
canv = document.getElementById 'canv'
ctx = canv.getContext "2d"
showprime = true
document.getElementById('showprime').checked = showprime

# helper function for mouse move handler
getElementTopLeft = ( id ) ->
    ele = document.getElementById(id);
    top = 0
    left = 0
    while ele.tagName != "BODY"
      top += ele.offsetTop
      left += ele.offsetLeft
      ele = ele.offsetParent
    return [top, left]

# call this to size the html elements if the canvas dimension changes.
setLayout = (e) ->
  canv.width = canvWidth;
  canv.height = canvWidth;
  [canvTop,canvLeft] = getElementTopLeft 'canv'

# clear the canvas and set the fill color for points
clear = () ->
  ctx.fillStyle = "#000"
  ctx.fillRect 0, 0, canv.width, canv.height
  ctx.fillStyle = "#fff"

# (experimental) draw a semi-transparent diagonal green line with specified 'slope' and right shift
# where 'slope' is +/- 1 in the sense of moving up the screen to the right.
# a shift of zero results in a line through the origin (center of canvas)
draw_line = (slope, shift) ->
  if shift > 0 
    x1 = shift
    x2 = canvWidth
    y1 = (canvWidth + slope*canvWidth)/2
    y2 = slope*shift + (1 - slope)/2*canvWidth
  else
    x1 = 0
    x2 = canvWidth - shift
    y1 = slope*shift + (1 + slope)/2*canvWidth
    y2 = (canvWidth - slope*canvWidth)/2
 
  ctx.strokeStyle = "rgba(0, 255, 0, 0.5)";
  ctx.beginPath();
  ctx.moveTo(x1, y1);
  ctx.lineTo(x2, y2);
  ctx.stroke();

drawPixel = ( x, y ) ->
  ctx.fillRect x, y, pixel, pixel

pushPrimes = (new_numbers, first) ->
  for i in [0...new_numbers.length]
    primes.push (i+first) if new_numbers[i] == 1


factorize = (start, max) ->
  new_numbers = []
  for i in [start..max]
    factors = 0
    ti = i
    for p in primes
      while ti > 1 and ti%p == 0
        factors++
        ti /= p
    factors = 1 if factors == 0
    new_numbers.push { _id: i, factors: factors}
    numbers.push factors
    primes.push i if factors == 1
  return new_numbers

# try to ensure that we have enough primes to fill the canvas
getNumbers = (max) ->
  nl = numbers.length
  return if nl > max - 2  
  # don't have enough numbers
  if server_present
    # get as many as we can from the server (if present)
    getRangeAjax nl, max

  nl = numbers.length
  return if nl > max - 2
  # still don't have enough -- compute them (and send them to the server if present).
  start = if nl > 2 then nl else 2

  new_numbers = factorize start, max

  if new_numbers.length > 0 and server_present
    postNewAjax new_numbers

getRangeAjax = (first, last) ->
  chunk = 20000
  start = first
  got_data = true
  dl = 0
  while last-start+1 > chunk and got_data
    dl = getChunkAjax start, (start+chunk-1)
    got_data = dl > 0
    start += dl
  if dl > 0 or last-first+1 <= chunk
    getChunkAjax start, last 

getChunkAjax = (first, last) ->
  dl = 0
  $.ajax(
    type: "GET"
    url: "/get_range/"
    async: false
    data: { first: first, last: last}
    ).done ( msg_data ) ->
      dl = msg_data.length
      if dl > 0
        numbers = numbers.concat msg_data
        pushPrimes msg_data, first
        console.log "Got " + dl + " numbers from the server."
  return dl
  
postNewAjax = (new_numbers) ->
  chunk = 5000
  start = 0
  left = new_numbers.length
  while left > chunk
    postChunkAjax new_numbers[start...start+chunk]
    start += chunk
    left -= chunk
  postChunkAjax new_numbers[start..]

postChunkAjax = (chunk) ->
    $.ajax(
      type: "POST"
      url: "/save/"
      async: false
      data: { numbers: chunk}
      ).done ( msg_data ) ->
        console.log "Saved to the server: " + msg_data

factorize = (start, max) ->
  new_numbers = []
  for i in [start..max]
    factors = 0
    ti = i
    for p in primes
      while ti > 1 and ti%p == 0
        factors++
        ti /= p
    factors = 1 if factors == 0
    new_numbers.push { _id: i, factors: factors}
    numbers.push factors
    primes.push i if factors == 1
  return new_numbers

# plot the spiral
spiral = (width) ->
  points = []
  nonprimes = document.getElementById('nonprimes').checked
  x = xmin = xmax = Math.floor width/2
  y = ymin = ymax = Math.floor(width / (if tri then (3/2) else 2))
  i = 1
  pidx = 0
  direction = 0
  pass = 1
  step = 1
  stepper = if tri then take_step_tri else take_step
  p = primes[pidx]
  while i < max_num
    return unless p
    prime_plotted = false
    while true
      [i, x, y, pass, step, direction, prime_plotted] = 
        walk_til_prime_or_corner i, p, x, y, pass, step, direction, stepper, nonprimes
      if prime_plotted
        p = primes[pidx++]
        break

# Walk the spiral in one direction until either a prime is encountered and plotted 
# or a turn is made (or both).
walk_til_prime_or_corner = (i, p, x, y, pass, step, direction, stepper, nonprimes) ->
  prime_plotted = false
  while i < p and step <= pass
    points.push [x,y]
    if nonprimes
      ctx.fillStyle = get_fill_style numbers[i]
      drawPixel x, y
      ctx.fillStyle = '#fff'
    i++
    step++
    [x, y] = stepper x, y, direction
  if (step > pass)
    direction = (direction + 1)%(if tri then 3 else 4)
    step = 1
    if tri or direction%2 == 0
      pass++
  if i >= p
    drawPixel x, y
    point_primes[x+","+y] = p
    prime_plotted = true
  return [i, x, y, pass, step, direction, prime_plotted]


get_fill_style = (factors) ->
  palette = document.getElementById('palette')
  if palette.value == "grayscale"
    # grayscale
    switch factors
      when 1 then '#fff'
      when 2 then '#eee'
      when 3 then '#ddd'
      when 4 then '#ccc'
      when 5 then '#bbb'
      when 6 then '#aaa'
      when 7 then '#999'
      when 8 then '#888'
      when 9 then '#777'
      when 10 then '#666'
      when 11 then '#555'
      when 12 then '#444'
      when 13 then '#333'
      when 14 then '#222'
      when 15 then '#111'
      else '#000'
  else
    # color
    switch factors
      when 1 then '#fff'
      when 2 then '#f47070'
      when 3 then '#f49970'
      when 4 then '#f4f470'
      when 5 then '#70f470'
      when 6 then '#7070f4'
      when 7 then '#9990f4'
      when 8 then '#7744f4'
      when 9 then '#3300f4'
      else '#000'

# Walk a single step.
#   Square spiral case
take_step = (x, y, direction) ->
  switch direction
    when 0 then x += 2*pixel
    when 1 then y += 2*pixel
    when 2 then x -= 2*pixel
    when 3 then y -= 2*pixel
  return [x, y]

#   Triangular spiral case
take_step_tri = (x, y, direction) ->
  switch direction
    when 0
      x += pixel
      y -= 2*pixel
    when 1
      x += pixel
      y += 2*pixel
    else
      x -= 2*pixel
  return [x, y]

# Get the primes and generate the plot and associated info
do_spiral = () ->

  max_num = if tri then (canvWidth+3*pixel)*(canvWidth+3*pixel) / Math.pow(2*pixel, 2) / 2
  else (canvWidth+2*pixel)*(canvWidth+2*pixel)/pixel/pixel/4

  getNumbers (max_num + 2*Math.ceil(Math.log(max_num))) # make sure we have enough primes...
  pl = primes.length
  totals = document.getElementById 'totals'
  totals.innerHTML = pl + " primes available.<br>The largest is: " + primes[pl-1] + ".<br>"
  clear()
  spiral canvWidth

# Inform the user of prime at mouse location, if any.
canv.onmousemove = (e) ->
  return true unless showprime
  tooltip = document.getElementById 'tooltip'
  tol = 1
  x0 = e.pageX - canvLeft
  y0 = e.pageY - canvTop
  for x in [x0-tol-pixel..x0+tol]
    for y in [y0-tol-pixel..y0+tol]
      prime = point_primes[x+","+y]
      break if prime
    break if prime
  tooltip.innerHTML = if prime then prime else ""
  tooltip.style.top = (e.pageY - 20) + "px"
  tooltip.style.left = (e.pageX + 10) + "px"
  return true

canv.onmouseout = (e) ->
  document.getElementById('tooltip').innerHTML = ""


document.getElementById('calculate').onclick = () ->
  point_primes = {}
  tri = document.getElementById('triangular').checked
  canvWidth = parseInt(document.getElementById('canvas_size').value)
  pixel = parseInt(document.getElementById('pixel_size').value)
  setLayout()
  do_spiral()
  return true

document.getElementById('btn_plot_num').onclick = () ->
  plot_num = parseInt document.getElementById('plot_num').value
  return true if plot_num == NaN or plot_num < 1 or plot_num > points.length
  ctx.fillStyle = '#f00'
  [x, y] = points[plot_num-1]
  drawPixel x, y
  ctx.fillStyle = '#fff'
  return true

document.getElementById('showprime').onclick = () ->
  showprime = this.checked
  document.getElementById('tooltip').innerHTML = "" unless showprime
  return true

window.onload = () ->
  setLayout()
  return true

